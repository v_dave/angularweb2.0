import { Component, OnInit, Inject } from '@angular/core';
import { Dish } from '../shared/dish';
import { Comment } from '../shared/comment';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import {DishService} from '../services/dish.service';
import 'rxjs/add/operator/switchMap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Feedback, ContactType } from '../shared/feedback';
import { visibility , flyInOut ,expand} from '../animations/app.animation';



@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
    },
    animations: [
      flyInOut(),
      visibility(),
      expand()
    ]
})
export class DishdetailComponent implements OnInit {
   
  dish : Dish;
  dishIds: number[];
  prev: number;
  next: number;
  feedbackForm: FormGroup;
  feedback: Feedback;
  contactType = ContactType;
  rating : number;
  comment : string;
  author : string;
  arr_comment : Comment;
  date : string;
  errMess: string;
  dishcopy = null;
  visibility = 'shown';


  constructor(private dishService:DishService, 
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    @Inject('BaseURL') private BaseURL
  ) {}

    ngOnInit() {
         this.createForm();       
          this.dishService.getDishIds().subscribe(dishIds => this.dishIds = dishIds);
          this.route.params
          .switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishService.getDish(+params['id']); })
          .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
              errmess => { this.dish = null; this.errMess = <any>errmess; });
          this.arr_comment = new Comment();

          }

  goBack(): void {
    this.location.back();
  }

  setPrevNext(dishId: number) {
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1)%this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds.length];
  }


  
  createForm() {
    this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],
      comment: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],
      rating: '1',
      message: ''
    });

    this.feedbackForm.valueChanges
    .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.feedbackForm) { return; }
    const form = this.feedbackForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      this.formValues[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
         for (const key in control.errors) {
           this.formErrors[field] += messages[key] + ' ';
         }
       
      }
      this.formValues[field] += control.value + ' ';
    }
    this.formValues['rating'] = form.get('rating').value + " stars";
    this.formValues['firstname'] =  form.get('firstname').value ;
    this.rating= Number(form.get('rating').value);
    this.comment =  this.formValues['comment'];
    this.author = this.formValues['firstname'];   
  }

myFunction() {
    this.arr_comment.author=this.author;
    this.arr_comment.rating=this.rating;
    this.arr_comment.comment=this.comment;
    this.arr_comment.date = new Date().toISOString();
  
    this.dishcopy.comments.push (this.arr_comment);
    this.dishcopy.save()
    .subscribe(dish => { this.dish = dish; console.log(this.dish); });
}


  formErrors = {
    'firstname': '', 
    'comment': '',  
  };


  formValues = {
    'firstname': '',
    'comment': '',
    'rating' : ''
  };

  validationMessages = {
    'firstname': {
      'required':      'First Name is required.',
      'minlength':     'First Name must be at least 2 characters long.',
      'maxlength':     'FirstName cannot be more than 25 characters long.'
    },
    'comment': {
      'required':      'comment is required.',
      'minlength':     'comment must be at least 2 characters long.',
      'maxlength':     'comment cannot be more than 25 characters long.'
    },
    
  };


  onSubmit() {
    this.feedback = this.feedbackForm.value;
    this.myFunction();
    console.log(this.feedback);
    this.feedbackForm.reset({
      firstname: '',
      comment: '',
      rating: '',
      message: ''
    });
  }


}